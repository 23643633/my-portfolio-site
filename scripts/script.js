$("a.nav-item").hover( //Used to toggle the dashed border that appears round the menu items
  function () {
    $(this).addClass("outer");
  },
  function () {
    $(this).removeClass("outer");
  }
);

function getPosition( element ) { //used to get the position of an element on the page
    var rect = element.getBoundingClientRect();
    return {x:rect.left,y:rect.top};// returns x and y co-ordinates of the element
}


menuButton = document.getElementsByClassName('menu-button');//Used to move the menu
menuButton[0].addEventListener('click', function(){// checks to see if the menu button is clicked
  var menu = document.querySelector('#alt'); //finds the alternative menu tag (not home page menu)
  var pos = getPosition(menu); // gets the position of the menu
  if (pos.y < 0) { // checks if the y position is above the screen
    menu.classList.remove('disable-css-transitions'); //removes the class that disables animations
  }else if(pos.y >= 200){//checks if the menu is on the screen
    menu.classList.add('disable-css-transitions');//removes the animation from the menu and resets its position
  }

});
